import sqlite3
conn = sqlite3.connect('employee.db')
c = conn.cursor()
c.execute("DROP TABLE IF EXISTS DEPARTMENTS")
c.execute("""CREATE TABLE DEPARTMENTS(
             DEPARTMENT_ID INTEGER NOT NULL,
             DEPARTMENT_NAME TEXT NOT NULL
)""")
conn.commit()

da_sql = "INSERT INTO DEPARTMENTS (DEPARTMENT_ID,DEPARTMENT_NAME) VALUES (?,?)"
c.execute(da_sql, (1,'Application Development'))
c.execute(da_sql, (2,'Marketing'))
c.execute(da_sql, (3,'Technical'))
c.execute(da_sql, (4,'HR'))
c.execute(da_sql, (7,'Finance'))

d=int(input("Enter department ID: "))
c.execute("SELECT * FROM EMPLOYEE E JOIN DEPARTMENTS D ON E.DEPARTMENT_ID=D.DEPARTMENT_ID AND D.DEPARTMENT_ID = {}".format(d))
print(c.fetchall())
conn.commit()
conn.close()