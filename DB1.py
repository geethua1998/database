import sqlite3
conn = sqlite3.connect('cars.db')
c = conn.cursor()
c.execute("DROP TABLE IF EXISTS CARS")
c.execute("""CREATE TABLE CARS(
            Car_Name TEXT,
           Owner_Name TEXT
)""")

conn.commit()

data_sql = "INSERT INTO CARS (Car_Name,Owner_Name) VALUES (?,?)"
c.execute(data_sql, ('Maruthi Suzuki Swift','Jace Herondale'))
c.execute(data_sql, ('Kia Seltos','Clarissa Fray'))
c.execute(data_sql, ('MG Hector','Tessa Gray'))
c.execute(data_sql, ('Ford EcoSport','Dorian Havilliard'))
c.execute(data_sql, ('Maruthi Suzuki Swift Dzire','Rose Hathaway'))
c.execute(data_sql, ('Renault KWID','Hazel Grace'))
c.execute(data_sql, ('Renault Duster','Ruby Santos'))
c.execute(data_sql, ('Maruthi Suzuki Baleno','David'))
c.execute(data_sql, ('Kia Sonet','Anna Chase'))
c.execute(data_sql, ('Ford Figo','Harper Wallis'))


c.execute("SELECT * FROM CARS")
print(c.fetchall())

conn.commit()
conn.close()