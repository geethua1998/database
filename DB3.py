import sqlite3
conn = sqlite3.connect('employee.db')
c = conn.cursor()
c.execute("DROP TABLE IF EXISTS EMPLOYEE")
c.execute("""CREATE TABLE EMPLOYEE(
             NAME TEXT NOT NULL,
             ID INTEGER NOT NULL,
             SALARY INTEGER,
             DEPARTMENT_ID INTEGER
)""")
conn.commit()

c.execute("ALTER TABLE EMPLOYEE ADD CITY TEXT")
conn.commit()

d_sql = "INSERT INTO EMPLOYEE (NAME,ID,SALARY,DEPARTMENT_ID,CITY) VALUES (?,?,?,?,?)"
c.execute(d_sql, ('Rowan',107,50000,1,'NEW YORK'))
c.execute(d_sql, ('Asher',354,35000,3,'LA'))
c.execute(d_sql, ('James',721,32000,7,'CHICAGO'))
c.execute(d_sql, ('Emma',412,39000,4,'NEW YORK'))
c.execute(d_sql, ('Will',225,45000,2,'MUMBAI'))

c.execute("SELECT NAME,ID,SALARY FROM EMPLOYEE")
print(c.fetchall())

c.execute("SELECT NAME FROM EMPLOYEE WHERE NAME LIKE 'J%'")
print(c.fetchall())
l=input("Enter first letter of employee's name: ").upper()
c.execute("SELECT NAME FROM EMPLOYEE WHERE NAME LIKE '{}%'".format(l))
print(c.fetchall())

i=int(input("Enter employee ID: "))
c.execute("SELECT * FROM EMPLOYEE WHERE ID='{}'".format(i))
print(c.fetchone())

u=int(input("To change name of an employee enter employee ID: "))
n=input("Enter new name: ")
c.execute("UPDATE EMPLOYEE SET NAME='{}' WHERE ID='{}'".format(n,u))
c.execute("SELECT * FROM EMPLOYEE")
print(c.fetchall())

conn.commit()
conn.close()
