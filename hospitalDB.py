import sqlite3
conn = sqlite3.connect('hospital.db')
c = conn.cursor()

#HOSPITAL TABLE
cursor.execute("DROP TABLE IF EXISTS HOSPITAL")
c.execute("""CREATE TABLE HOSPITAL(
            Hospital_Id INTEGER PRIMARY KEY NOT NULL,
            Hospital_Name TEXT,
            Bed_Count INTEGER NOT NULL
)""")

#DOCTOR TABLE
cursor.execute("DROP TABLE IF EXISTS DOCTORS")
c.execute("""CREATE TABLE DOCTOR(
             Doctor_Id INTEGER PRIMARY KEY NOT NULL,
             Doctor_Name TEXT,
             Hospital_Id INTEGER NOT NULL,
             Joining_Date TEXT NOT NULL,
            Speciality TEXT,
             Salary INTEGER,
            Experience INTEGER
)""")

conn.commit()
query_1 = "INSERT INTO HOSPITAL (Hospital_Id,Hospital_Name,Bed_Count) VALUES(?,?,?)"
cursor.execute(query_1, (1, 'Mayo Clinic', 200))
cursor.execute(query_1, (2, 'Cleveland Clinic', 400))
cursor.execute(query_1, (3, 'Johns Hopkins', 1000))
cursor.execute(query_1, (4, 'UCLA Medical Center', 1500))

query_2 = "INSERT INTO DOCTORS (Doctor_Id,Doctor_Name,Hospital_Id,Joining_Date,Speciality,Salary,Experience) VALUES(?,?,?,?,?,?,?)"
cursor.execute(query_2,(101, 'David',1,'2005-02-10','Pediatric',40000,'NULL'))
cursor.execute(query_2,(102, 'Michael',1,'2018-07-23','Oncologist',20000,'NULL'))
cursor.execute(query_2,(103, 'Susan',2,'2016-05-19','Garnacologist',25000,'NULL'))
cursor.execute(query_2,(104, 'Robert',2,'2017-12-28','Pediatric',28000,'NULL'))
cursor.execute(query_2,(105, 'Linda',3,'2004-06-04','Garnacologist',42000,'NULL'))
cursor.execute(query_2,(106, 'William',3,'2012-09-11','Dermatologist',30000,'NULL'))
cursor.execute(query_2,(107, 'Richard',4,'2014-08-21','Garnacologist',32000,'NULL'))
cursor.execute(query_2,(108, 'Karen',4,'2011-10-17','Radiologist',30000,'NULL'))
conn.commit()



#LIST OF DOCTORS AS PER SPECIALITY AND SALARY
spec=['Pediatric','Oncologist','Garnacologist','Dermatologist','Radiologist']
s=''
while s not in spec:
    s=input('Enter speciality: ').capitalize()
sal=int(input('Enter salary: '))
def specialist_doctors_list(speciality, salary):
        conn = sqlite3.connect('hospital.db')
        c = conn.cursor()
        sql_select_query = """SELECT * FROM DOCTOR WHERE Speciality = ? AND Salary >= ?"""
        c.execute(sql_select_query, (speciality, salary))
        print("Printing doctors whose specialty is", speciality, "and salary greater than", salary, "\n")
        records = c.fetchall()
        print(c.fetchall())
        for i in records:
            print("Doctor Id: ", i[0])
            print("Doctor Name:", i[1])
            print("Hospital Id:", i[2])
            print("Joining Date:", i[3])
            print("Specialty:", i[4])
            print("Salary:", i[5])
            print("Experience:", i[6], "\n")
        conn.commit()
        conn.close()
specialist_doctors_list(s,sal)

#DOCTORS AS PER HOSPITAL ID
def hospital_name(hospital_id):
    conn = sqlite3.connect('hospital.db')
    c = conn.cursor()
    id_query = """select * from Hospital where Hospital_Id = ?"""
    c.execute(id_query, (hospital_id,))
    l = c.fetchone()
    conn.commit()
    conn.close()
    return l[1]
def doctors(hospital_id):
    hname = hospital_name(hospital_id)
    conn = sqlite3.connect('hospital.db')
    c = conn.cursor()
    sql_query = """select * from Doctor where Hospital_Id = ?"""
    c.execute(sql_query, (hospital_id,))
    h = c.fetchall()
    print("Printing Doctors of ", hname, "Hospital")
    for i in h:
        print("Doctor Id: ", i[0])
        print("Doctor Name:", i[1])
        print("Hospital Id:", i[2])
        print("Joining Date:", i[3])
        print("Specialty:", i[4])
        print("Salary:", i[5])
        print("Experience:", i[6], "\n")
    conn.commit()
    conn.close()
i=int(input('Enter hospital ID: '))
doctors(i)

conn.commit()
conn.close()